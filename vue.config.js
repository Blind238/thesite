// const path = require('path')
// const PrerenderPlugin = require('prerender-spa-plugin')

module.exports = {
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title = 'Portfolio | Jeremy Granadillo'
        return args
      })
  }
  // },
  // configureWebpack: () => {
  //   if (process.env.NODE_ENV !== 'production') return
  //   return {
  //     plugins: [
  //       new PrerenderPlugin(
  //         // aboslute path to built SPA
  //         path.resolve(__dirname, 'dist'),
  //         // list of routes to prerender
  //         ['/'],
  //         // options
  //         {
  //         }
  //       )
  //     ]
  //   }
  // }
}
